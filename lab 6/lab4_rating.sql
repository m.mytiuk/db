﻿use reiting

--1
create proc AvgMarksWithoutRepass
@periodBegin Date, @periodEnd Date as
begin
	select kod_student, avg(Reiting.Reiting) as rating
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		where Rozklad_pids.Date between @periodBegin and @periodEnd
				and Zdacha_type = 1
		group by kod_student
end

declare @pBegin date = '01/01/2018'
declare @pEnd date = '12/31/2018'

exec AvgMarksWithoutRepass @pBegin, @pEnd

--2
create proc AvgMarksWithRepass
@periodBegin Date, @periodEnd Date as
begin
	select kod_student, avg(Reiting) as rating
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		where Rozklad_pids.Date between @periodBegin and @periodEnd
				and Zdacha_type = 0
		group by kod_student
end

declare @pBegin date = '01/01/2018'
declare @pEnd date = '12/31/2018'
exec AvgMarksWithRepass @pBegin, @pEnd

--3
alter proc GetGoodStudents as
	begin
		select kod_student
			from Reiting
			group by Kod_student
			having min(Reiting) > 73
	end

exec GetGoodStudents

--4
create proc GetConvertedMarks as
begin
	declare @tmpTable as table(kod_student int, nationalMark int, ectsMark varchar(2));
	insert into @tmpTable(kod_student, nationalMark)
		select kod_student, avg(Reiting) as nationalMark
			from Reiting
			group by Kod_student

	declare @natMark int
	declare @ectsMark varchar(2)
	declare cur cursor for 
		select nationalMark from @tmpTable
	open cur
	fetch next from cur into @natMark
	while @@FETCH_STATUS = 0
		begin
			exec ConvertToECTS @ectsMark output, @natMark
			update top(1) @tmpTable
				set ectsMark = @ectsMark
				where ectsMark is NULL
			fetch next from cur into @natMark
		end
	close cur
	deallocate cur
	
	select * from @tmpTable
end

create proc ConvertToECTS 
	@ectsVal varchar(2) output, @nationalVal int as
	begin
		if @nationalVal >= 90
			set @ectsVal = 'A'
		else if @nationalVal >= 82
			set @ectsVal = 'B'
		else if @nationalVal >=74
			set @ectsVal = 'C'
		else if @nationalVal >=67
			set @ectsVal = 'D'
		else if @nationalVal >= 60
					set @ectsVal = 'E'
		else if @nationalVal >= 35
					set @ectsVal = 'FX'
		else if @nationalVal < 35
					set @ectsVal = 'F'

	end

exec GetConvertedMarks

--5
create trigger AddNewGroupIfNotExists on dbo_student instead of insert as
begin
	declare insertCur cursor for
		select kod_stud, Kod_group from inserted
	declare @studentId as int
	declare @groupId as varchar(5)
	declare @randomNavchPlan as int
	open insertCur
	fetch next from insertCur into @studentId, @groupId
	while @@FETCH_STATUS = 0
	begin
		if (select count(kod_group)
				from dbo_groups
				where Kod_group = @groupId) = 0
		begin
			set @randomNavchPlan = (select top(1) k_navch_plan from Navch_plan
										 order by rand())
			insert into dbo_groups(Kod_group, K_navch_plan)
				values(@groupId, @randomNavchPlan)
		end
		insert into dbo_student(Sname, Name, Fname, N_ingroup, Kod_group)
			select Sname, Name, Fname, N_ingroup, Kod_group
				from inserted
				where Kod_stud = @studentId
		fetch next from insertCur into @studentId, @groupId
	end

	close insertCur
	deallocate insertCur
end

insert into dbo_student(Kod_group)
	values(444)
select * from dbo_groups

--6
create trigger DeleteGroupIfEmpty on dbo_student for update as
begin
	declare insertCur cursor for
		select kod_stud, Kod_group from deleted
	declare @studentId as int
	declare @groupId as varchar(5)
	open insertCur
	fetch next from insertCur into @studentId, @groupId
	while @@FETCH_STATUS = 0
	begin
			update s
			set s.Sname = i.Sname, 
				s.Name = i.Name,
				s.Fname = i.Fname, 
				s.N_ingroup = i.N_ingroup, 
				s.Kod_group = i.Kod_group
			from dbo_student as s
				inner join inserted as i on s.Kod_stud = i.kod_stud
			where s.kod_stud = @studentId
			if (select count(*)
				from dbo_student
				where Kod_group = @groupId) = 0
			begin
				delete from dbo_groups
					where Kod_group = @groupId
			end
		fetch next from insertCur into @studentId, @groupId
	end

	close insertCur
	deallocate insertCur
end
