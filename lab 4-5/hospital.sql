﻿use Hospital

-- 3.1
select doctor_id, cabinet_id, time_start, time_end from Graphics

--3.2
select Patients.patient_id, patient_fullname
	from Patients
	inner join Visits on Patients.patient_id = Visits.patient_id
	where doctor_id = 5 and time between '06/01/2021' and '06/30/2021'

--3.3
select count(Diseases.disease_id) as cases_count, disease
	from Diseases
	inner join Visits on Diseases.disease_id = Visits.disease_id
	where time between '06/01/2021' and '06/30/2021'
	group by disease

--3.4
	select doctor_id, count(Visits.type_id) as visit_types_cases, type_name
		from Visits
		inner join VisitTypes on Visits.type_id = VisitTypes.type_id
		group by doctor_id, type_name
		order by doctor_id

--3.5
insert into Cabinets(cabinet_id, room)
	values (7, 310)
update Cabinets
	set room = 404
	where cabinet_id = 2

insert into Diseases(disease_id, disease)
	values (7, N'Фарингіт')
update Diseases
	set disease = N'Тонзиліт'
	where disease_id = 5

insert into Doctors(doctor_id, doctor_fullname, speciality, birthday_date)
	values (7, N'Кузменко І.А', N'ЛОР', '05/08/1989')
update Doctors
	set speciality = N'Кардіолог'
	where doctor_id = 4

--3.6
insert into Visits (visit_id, doctor_id, patient_id, cabinet_id, time, type_id, disease_id)
	values (11, 4, 3, 4, '08/06/2021 15:45', 1, 3),
			(12, 4, 3, 4, '10/06/2021 12:10', 2, 3),
			(13, 3, 1, 3, '06/12/2021 16:00', 1, 7)

update Visits
	set cabinet_id = 5
	where doctor_id = 1

-- 3.7
select Doctors.doctor_id, doctor_fullname
	from Doctors
	inner join Graphics on Graphics.doctor_id = Doctors.doctor_id
	where time_start > CAST('12:00:00' AS time)

select count(Visits.visit_id) as overtime_visits_count, Doctors.doctor_id, doctor_fullname
	from Doctors
	inner join Graphics on Graphics.doctor_id = Doctors.doctor_id
	inner join Visits on Visits.doctor_id = Doctors.doctor_id
	where time_start > CAST(Visits.time AS time) or time_end <  CAST(Visits.time AS time)
	group by Doctors.doctor_id, doctor_fullname
