﻿use reiting

-- 2.1
select Reiting.Kod_student, Predmet_plan.K_predmet, sum(Reiting.Reiting) as rateingSum
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by Reiting.Kod_student, Predmet_plan.K_predmet
-- 2.2
select kod_group, count(Kod_stud) as studentsNumber
		from dbo_student
		group by Kod_group

-- 2.3
select kod_group, count(K_predmet) as predmetCount
		from Rozklad_pids
		inner join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
		group by kod_group

-- 2.4
select kod_group, count(K_zapis) as classesCount
		from Rozklad_pids
		group by kod_group

-- 2.5
select kod_group, avg(Reiting) as avgGroupMark
		from Rozklad_pids
		inner join Reiting on Rozklad_pids.K_zapis = Reiting.K_zapis
		group by kod_group

--2.6
select K_predmet, avg(Reiting) as avgPredmetMark
		from Reiting
		inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
		inner join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
		group by K_predmet

-- 2.7
select kod_student, k_predmet, avg(Reiting) as currentRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by kod_student, k_predmet

-- 2.8
select k_predmet, min(Reiting) as minRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet

-- 2.9
select k_predmet, max(Reiting) as maxRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet

-- 2.10
select k_predmet, Zdacha_type, count(K_zapis) as classesCount
		from Rozklad_pids
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet, Zdacha_type

-- 2.11
select Spetsialnost.K_spets, count(distinct dbo_groups.Kod_group) as groupsCount
		from dbo_groups
		inner join Rozklad_pids on dbo_groups.Kod_group = Rozklad_pids.Kod_group
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		inner join Navch_plan on Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan
		inner join Spetsialnost on Navch_plan.K_spets = Spetsialnost.K_spets
		group by Spetsialnost.K_spets

--2.12
delete Reiting from Reiting
	inner join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student
	where dbo_student.Sname = N'Краснюк'

-- 2.13
update Reiting set Reiting = (Reiting * 1.15)

-- 2.14
update Reiting set Reiting = (Reiting * 0.85)

--2.15
delete dbo_student from dbo_student
	where Kod_group = N'ПІ-53'

--2.16
update Reiting set Prisutn = 1
	from Reiting
    inner join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student
	where dbo_student.Sname = N'Гриневич'
		
