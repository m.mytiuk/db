﻿use torg_firm

-- 1.1
select count(NaSklade) as goodsCount from tovar
-- 1.2
select count(*) as employeesCount from sotrudnik
-- 1.3
select count(*) as providerCount from postachalnik
-- 1.4
select id_tovar, sum(kilkist) as productsCount
	from zakaz_tovar 
	inner join zakaz on zakaz_tovar.id_zakaz = zakaz.id_zakaz
	where month(date_rozm) = month(getdate())
	group by id_tovar
--1.5
select sum(kilkist*price) as month_sum
		from zakaz_tovar 
		inner join zakaz on zakaz_tovar.id_zakaz = zakaz.id_zakaz
		inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
		where month(date_rozm) = month(getdate())
--1.6
select id_postav, sum(kilkist*price) - sum(kilkist*price)*Znigka/100 as moneyMonthly
		from zakaz_tovar 
		inner join zakaz on zakaz_tovar.id_zakaz = zakaz.id_zakaz
		inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
		group by id_postav, Znigka

--1.7
select id_postav, count(id_zakaz) as zakazAmount
		from zakaz_tovar
		inner join (select id_postav, id_tovar
						from tovar
						where nazva = N'Молоко') as milk
				on milk.id_tovar = zakaz_tovar.id_tovar
		group by id_postav
--1.8
select tovar.id_tovar, tovar.Nazva, avg(price * kilkist) as avgSum
		from zakaz_tovar
		inner join tovar on tovar.id_tovar = zakaz_tovar.id_tovar
		group by tovar.id_tovar, tovar.Nazva
--1.9
select zakaz.id_klient, city, sum(Price*Kilkist) as cost
		from klient
		inner join zakaz on klient.id_klient = zakaz.id_klient
		inner join zakaz_tovar on zakaz.id_zakaz = zakaz_tovar.id_zakaz
		inner join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
		where city = N'Житомир'
		group by zakaz.id_klient, city
--1.10
select postachalnik.id_postach, avg(price) as avgPrice
		from tovar
		inner join postachalnik on postachalnik.id_postach = tovar.id_tovar
		group by postachalnik.id_postach
